# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop
require python [ multibuild=false blacklist=2 ]
require meson

SUMMARY="Media Player for the GNOME Desktop"

PLATFORMS="~amd64 ~x86"

LICENCES="GPL-2"
SLOT="1.0"

MYOPTIONS="
    gtk-doc
    python [[ description = [ Add support for plugins written in python ] ]]
"

# opensubtitles
# zeitgeist-dp

# Most plugins rely on automagic dependencies :(
DEPENDENCIES="
    build:
        dev-lang/perl:* [[ note = [ for pod2man ] ]]
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.43.4]
        dev-libs/libpeas:1.0[>=1.1.0]
        gnome-desktop/gnome-desktop:3.0
        gnome-desktop/gobject-introspection:1[>=0.6.7]
        gnome-desktop/grilo:0.3[>=0.3]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/totem-pl-parser[>=3.10.1][gobject-introspection]
        media-libs/clutter-gst:3.0[>=2.99.2]
        media-libs/gstreamer:1.0[>=1.6.0]
        media-plugins/gst-plugins-base:1.0[>=1.6.0] [[
            note = [ for gstreamer-{tag,audio,video}, gstreamer-pbutils; plugins: playbin, videoscale ]
        ]]
        media-plugins/gst-plugins-good:1.0 [[
            note = [ for plugins: autoaudiosink, scaletempo, goom ]
        ]]
        media-plugins/gst-plugins-bad:1.0
        x11-libs/cairo[>=1.14.0]
        x11-libs/clutter:1[>=1.17.3]
        x11-libs/clutter-gtk:1.0[>=1.8.1]
        x11-libs/cogl [[ note = [ for rotation plugin ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.19.4][gobject-introspection]
        python? (
            dev-python/pylint[python_abis:*(-)?]
            gnome-bindings/pygobject:3[>=2.90.3][python_abis:*(-)?]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-easy-codec-installation=yes
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'
    'python python yes no'
)

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

