# Copyright 2013 Nathan Maxson <joyfulmantis@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'geocode-glib-0.99.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="GLib geocoding library that uses the Yahoo! Place Finder service"
DESCRIPTION="
geocode-glib is a convenience library for the geocoding (finding longitude,
and latitude from an address) and reverse geocoding (finding an address from
coordinates). It uses Nominatim service to achieve that. It also caches
(reverse-)geocoding requests for faster results and to avoid unnecessary server
load.
"
HOMEPAGE="https://git.gnome.org/browse/geocode-glib/"

LICENCES="GPL-2"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection gtk-doc"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        core/json-glib[>=0.99.2][gobject-introspection?]
        dev-libs/glib:2[>=2.44]
        gnome-desktop/libsoup:2.4[>=2.42][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.3] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-installed-tests=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    "gobject-introspection enable-introspection"
    "gtk-doc enable-gtk-doc"
)

