# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require meson

SUMMARY="HTTP library implemented in C"
HOMEPAGE="http://live.gnome.org/LibSoup"

LICENCES="LGPL-2"
SLOT="2.4"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    brotli
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( linguas: an as be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fa fr fur gl gu he hi
               hu id it ja kn ko lt lv ml mr nb nl or pa pl pt pt_BR ro ru sk sl sr sr@latin sv ta
               te tg th tr ug uk uz@cyrillic vi zh_CN zh_HK zh_TW )
"

# network violations
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.58.0]
        dev-libs/libpsl[>=0.20]
        dev-libs/libxml2:2.0
        sys-libs/zlib
        brotli? ( app-arch/brotli )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    run:
        dev-libs/glib-networking[ssl(+)]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgnome=true
    -Dgssapi=disabled
    -Dntlm=disabled
    -Dntlm_auth=false
    -Dsysprof=disabled
    -Dinstalled_tests=false
    # To make glib-networking a runtime dependency
    # Also ease cross-compiling a bit
    -Dtls_check=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    brotli
    'gobject-introspection introspection'
    vapi
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

