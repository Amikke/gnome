# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require systemd-service pam
require gsettings
require gtk-icon-cache
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]
require s6-rc-service

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="GTK+ based login manager"
HOMEPAGE="http://live.gnome.org/GDM"

LICENCES="GPL-2"
SLOT="0"

DEPENDENCIES="
    build+run:
        group/gdm
        user/gdm
        gnome-desktop/dconf
    suggestion:
        x11-server/xorg-server [[ description = [ Requires an X server ] ]]
"

gdm_src_install() {
    gsettings_src_install

    keepdir /var/lib/gdm            # GDM_WORKING_DIR
    keepdir /var/log/gdm            # GDM_LOG_DIR
    keepdir /var/cache/gdm

    # GDM_XAUTH_DIR is created automatically
    edo rmdir "${IMAGE}"/run/{gdm/,}

    edo chmod g+w "${IMAGE}"/var/lib/gdm
    edo chown gdm:gdm "${IMAGE}"/var/lib/gdm

    # dconf lock directory
    keepdir /etc/dconf/db/gdm.d/locks

    edo rmdir "${IMAGE}/var/lib/gdm/.local/share/applications"  \
              "${IMAGE}/var/lib/gdm/.local/share"               \
              "${IMAGE}/var/lib/gdm/.local"

    install_openrc_files
    install_s6-rc_files
}

gdm_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    edo dconf update
}

gdm_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    edo dconf update
}

