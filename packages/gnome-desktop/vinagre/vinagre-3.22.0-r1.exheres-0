# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache vala [ vala_dep=true ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A VNC client for the GNOME desktop"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    avahi
    rdp       [[ description = [ build RDP plugin for vinagre ] ]]
    spice     [[ description = [ build SPICE plugin for vinagre ] ]]
    ssh       [[ description = [ build SSH plugin for vinagre ] ]]
    telepathy [[ description = [ build im-status plugin ] ]]
    ( linguas: ar as ast be be@latin bg bn bn_IN ca ca@valencia cs da de el en@shaw en_GB eo es et
               eu fa fi fr ga gl gu he hi hu id it ja kk kn ko ku lt lv mai mk ml mr ms nb nl nn oc
               or pa pl pt pt_BR ro ru si sk sl sq sr sr@latin sv ta te tg th tr ug uk vi zh_CN
               zh_HK zh_TW )
"

# appstream validation fails and --disable-appstream-util has no effect
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/appstream-glib
        dev-util/intltool[>=0.50.0]
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.24]
    build+run:
        dev-libs/glib:2[>=2.32.0]
        dev-libs/gtk-vnc:2.0[>=0.4.3]
        dev-libs/libsecret:1
        dev-libs/libxml2:2.0[>=2.6.31]
        x11-libs/gtk+:3[>=3.9.6]
        avahi? ( net-dns/avahi[>=0.6.26][dbus][gtk3][gobject-introspection] )
        rdp? (
            net-remote/rdesktop
            net-remote/FreeRDP[>=2.0]
        )
        spice? ( virtualization-ui/spice-gtk:3.0[>=0.5] )
        ssh? ( dev-libs/vte:2.91[>=0.20] )
        telepathy? (
            dev-libs/dbus-glib:1
            net-im/telepathy-glib[>=0.11.6]
        )
        !gnome-desktop/vinagre:1.0 [[
            description = [ slot moved ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/e41759e91f80c8dfebd4424373c7bf201fb10bf9.patch
    "${FILES}"/7f2166ec271a7a2d4ff2360e5cae1ecc8f5b66d6.patch
    "${FILES}"/9d52ad6384ad3ca34a2b32b71fd8c4cbbdbbba42.patch
    "${FILES}"/1790370c3c2765076077e8b36a19543698e27172.patch
    "${FILES}"/81f235f9bdb0e46f362e14811fbe83759232ba37.patch
    "${FILES}"/0001-Fix-compilation-with-GCC-10-fno-common.patch
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( avahi telepathy )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( rdp spice ssh )

src_prepare() {
    # Disable old gnome-common macros
    edo sed \
        -e 's|GNOME_COMPILE_WARNINGS|dnl &|' \
        -e 's|GNOME_MAINTAINER_MODE_DEFINES|dnl &|' \
        -i configure.ac

    autotools_src_prepare
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

