# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache
require meson

SUMMARY="Settings Daemon"
HOMEPAGE="http://www.gnome.org/"

LICENCES="GPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="cups
    wayland
    wwan
    ( linguas: af am ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs cy da de dz el
               en_CA en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka km kn ko
               ku lt lv mai mg mk ml mn mr ms nb nds ne nl nn nso oc or pa pl pt pt_BR ro ru rw si sk
               sl sq sr sr@latin sv ta te tg th tr ug uk vi wa xh zh_CN zh_HK zh_TW zu )
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.37.1]
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.56.0]
        dev-libs/nss[>=3.11.2]
        gnome-desktop/geocode-glib:1.0[>=3.10.0]
        gnome-desktop/gnome-desktop:3.0[>=3.37.1]
        gnome-desktop/gsettings-desktop-schemas[>=40.0]
        gnome-desktop/libgudev
        gnome-desktop/libgweather:3.0[>=40.0]
        gps/geoclue:2.0[>=2.3.1]
        media-libs/fontconfig
        media-libs/lcms2[>=2.2]
        media-libs/libcanberra[providers:gtk3]
        media-sound/pulseaudio[>=2.0]
        net-apps/NetworkManager[>=1.0]
        sys-apps/colord[>=1.4.5]
        sys-apps/upower[>=0.99.8][providers:elogind?][providers:systemd?]
        sys-auth/polkit:1[>=0.114]
        sys-sound/alsa-lib
        x11-apps/xkeyboard-config
        x11-libs/gtk+:3[>=3.15.3]
        x11-libs/libnotify[>=0.7.3]
        x11-libs/libX11
        x11-libs/libxkbfile
        x11-libs/libXext
        x11-libs/libXi
        (
            x11-libs/libwacom[>=0.7]
            x11-libs/pango[>=1.20.0]
        ) [[ *note = [ no way to disable this unnecessary dependency ] ]]
        cups? ( net-print/cups )
        wayland? ( sys-libs/wayland )
        wwan? (
            gnome-desktop/gcr
            net-wireless/ModemManager
        )
    recommendation:
        sys-apps/udisks:2 [[ description = [ support for disk management ] ]]
        providers:elogind? ( sys-auth/elogind ) [[ description = [ support power management functionality ] ]]
        providers:systemd? ( sys-apps/systemd[>=243] ) [[ description = [ support power management functionality ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dalsa=true'
    '-Dcolord=true'
    '-Dgudev=true'
    '-Drfkill=true'
    '-Dsmartcard=true'
    '-Dusb-protection=true'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'cups'
    'providers:systemd systemd'
    'wayland'
    'wwan'
)

# requires X
RESTRICT="test"

src_prepare() {
    meson_src_prepare

    edo sed -e "/systemd_userunitdir/s/gsd_prefix/'\/usr\/$(exhost --target)'/" \
            -i meson.build
}

src_install() {
    export GSETTINGS_DISABLE_SCHEMAS_COMPILE=1
    meson_src_install
    unset GSETTINGS_DISABLE_SCHEMAS_COMPILE

    keepdir "/etc/gnome-settings-daemon/xrandr"
}

pkg_postinst() {
    gsettings_exlib_compile_gsettings_schemas
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_exlib_compile_gsettings_schemas
    gtk-icon-cache_pkg_postrm
}

