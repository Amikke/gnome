# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true ]
require gsettings test-dbus-daemon

SUMMARY="A tool designed to extract information and metadata about your personal data"
HOMEPAGE="http://www.tracker-project.org/"

LICENCES="GPL-2 LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"

#    gstreamer [[ description = [ use gstreamer for media extraction ] ]]
MYOPTIONS="
    cuesheet [[ description = [ enable external cuesheet parsing ] ]]
    evolution
    exif
    firefox [[ description = [ build firefox miner ] ]]
    flac [[ description = [ use libflac for flac extraction ] ]]
    gif [[ description = [ use giflib for gif extraction ] ]]
    gobject-introspection
    gtk-doc
    jpeg [[ description = [ use libjpeg for jpeg extraction ] ]]
    media-art [[ description = [ use libmediaart to cache published video/audio ] ]]
    ms-office [[ description = [ use libgsf for Office document extraction ] ]]
    nautilus
    needle [[ description = [ build tracker-needle ] ]]
    networkmanager
    pdf [[ description = [ support PDF extraction ] ]]
    playlist [[ description = [ use libtotm-pl-parser for playlist extraction ] ]]
    preferences [[ description = [ build tracker-preferences ] ]]
    taglib [[ description = [ support taglib for auto writeback ] ]]
    thunderbird [[ description = [ build thunderbird miner ] ]]
    tiff [[ description = [ use libtiff for tiff extraction ] ]]
    upower [[ description = [ support battery/power detection ] ]]
    vorbis [[ description = [ use libvorbis for vorbis extraction ] ]]
    xmp [[ description = [ Extractor for XMP data ] ]]
    ( linguas: ar be@latin ca ca@valencia cs da de dz el en_GB eo es et fi fr gl he hu id it ja ko
               lt lv mk ml nb nds nl oc pa pl pt pt_BR ro ru sk sl sr sr@latin sv te th zh_CN zh_HK
               zh_TW )
    jpeg? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )"

#        gstreamer? ( media-libs/gstreamer:1.0[>=0.10.31]
#                     media-plugins/gst-plugins-base:1.0[>=0.10.31] )
DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.8] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    build+run:
        base/libgee:0.8[>=0.3] [[ note = [ automagic dependency for tracker-resdump ] ]]
        core/json-glib
        dev-libs/glib:2[>=2.44.0]
        dev-db/sqlite:3[>=3.7.15]
        dev-libs/icu:=[>=4.8.1.1] [[ note = [ consider switch to libunicode by default? ] ]]
        dev-libs/libxml2:2.0[>=2.6] [[ note = [ for xml/html extraction, perhaps optionalise? ] ]]
        gnome-desktop/libsoup:2.4[>=2.40]
        media-libs/gstreamer:1.0[>=0.10.31]
        media-plugins/gst-plugins-base:1.0[>=0.10.31]
        media-libs/libmediaart:2.0[>=1.9.0][gobject-introspection?]
        media-libs/libpng:=[>=1.2]
        sys-apps/dbus[>=1.3.1]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        sys-libs/libseccomp[>=2.0] [[ note = [ required for sandboxed metadata extraction ] ]]
        x11-libs/gdk-pixbuf:2.0[>=2.12.0]
        cuesheet? ( media-libs/libcue )
        evolution? ( mail-client/evolution[>=3.0.0]
                     gnome-desktop/evolution-data-server:1.2[>=2.32.0] )
        exif? ( media-libs/libexif[>=0.6] )
        firefox? ( net-www/firefox[=5.0*] )
        flac? ( media-libs/flac[>=1.2.1] )
        gif? ( media-libs/giflib:= )
        jpeg? ( providers:ijg-jpeg? ( media-libs/jpeg:= )
                providers:jpeg-turbo? ( media-libs/libjpeg-turbo ) )
        media-art? ( media-libs/libmediaart:2.0[>=1.9.0][gobject-introspection?] )
        ms-office? ( office-libs/libgsf:1[>=1.14.24] )
        nautilus? ( gnome-desktop/nautilus )
        needle? ( x11-libs/gtk+:3[>=3.0.0] )
        networkmanager? ( net-apps/NetworkManager[>=0.8] )
        pdf? ( app-text/poppler[glib][>=0.16.0]
               dev-libs/libgxps )
        playlist? ( gnome-desktop/totem-pl-parser )
        preferences? ( x11-libs/gtk+:3[>=3.0.0] )
        taglib? ( media-libs/taglib[>=1.6] )
        thunderbird? ( mail-client/thunderbird[=5.0*] )
        tiff? ( media-libs/tiff )
        upower? ( sys-apps/upower[>=0.9.0] )
        vorbis? ( media-libs/libvorbis[>=0.22] )
        xmp? ( media-libs/exempi:2.0[>=2.1.0] )
        linguas:ru? ( app-text/enca[>=1.9] )
       !app-pim/tracker[<0.17] [[ description = [ Many file collissions ] ]]
"

RESTRICT=test

# iso requires libosinfo
# tracker-miner-rss requires libgrss:0.7

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-dvi'
    '--enable-generic-media-extractor=gstreamer'
    '--enable-guarantee-metadata'
    '--enable-icon'
    '--enable-journal'
    '--enable-libpng'
    '--enable-libxml2'
    '--enable-miner-apps'
    '--enable-miner-fs'
    '--enable-miner-user-guides'
    '--enable-mp3'
    '--enable-ps'
    '--enable-text'
    '--enable-tracker-fts'
    '--enable-unzip-psgz-files'

    '--disable-abiword'
    '--disable-artwork'
    '--disable-guarantee-metadata'
    '--disable-hal'
    '--disable-libiptcdata'
    '--disable-libosinfo'
    '--disable-libstemmer'
    '--disable-maemo'
    '--disable-meegotouch'
    '--disable-miner-rss'

    '--with-gstreamer-backend=discoverer'
    '--with-unicode-support=libicu'

    '--disable-schemas-compile'
)

#                                       'gstreamer generic-media-extractor gstreamer'
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'cuesheet libcue'
                                       'evolution miner-evolution'
                                       'exif libexif'
                                       'firefox miner-firefox'
                                       'flac libflac'
                                       'gif libgif'
                                       'gobject-introspection introspection'
                                       'gtk-doc'
                                       'jpeg libjpeg'
                                       'media-art libmediaart'
                                       'ms-office libgsf'
                                       'nautilus nautilus-extension'
                                       'needle tracker-needle'
                                       'networkmanager network-manager'
                                       'pdf poppler'
                                       'pdf libgxps'
                                       'playlist'
                                       'preferences tracker-preferences'
                                       'taglib'
                                       'thunderbird miner-thunderbird'
                                       'tiff libtiff'
                                       'upower'
                                       'vorbis libvorbis'
                                       'xmp exempi'
                                       'linguas:ru enca' )

#DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'gstreamer gstreamer-backend discoverer' )

src_prepare() {
    default
    edo intltoolize --force --automake
}

src_test() {
    local success=

    unset DISPLAY

    # redirect any configuration and user settings to temp via XDG variables
    export XDG_DATA_HOME=${TEMP}
    export XDG_CACHE_HOME=${TEMP}
    export XDG_CONFIG_HOME=${TEMP}

    # G_DBUS_COOKIE_SHA1_KEYRING_DIR requires 0700, ${TEMP} is 0755
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR_IGNORE_PERMISSION=1
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR=${TEMP}

    # use the memory backend for settings to ensure that the system settings in dconf remain
    # untouched by the tests
    export GSETTINGS_BACKEND=memory

    # the tracker-dbus/request test relies on g_debug() messages being output to stdout
    export G_MESSAGES_DEBUG=all

    TZ=UTC LANG=C LC_ALL=C HOME="${TEMP}" test-dbus-daemon_run-tests
    success=${?}

    [[ ${success} == 0 ]] || die "emake check failed"
}

src_install() {
    gsettings_src_install

    keepdir "/usr/$(exhost --target)/lib/tracker-${SLOT}/writeback-modules"
    edo rm "${IMAGE}"/usr/share/tracker-tests/01-writeback.py
    edo find "${IMAGE}" -type d -empty -delete
}

