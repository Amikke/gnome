# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true ]
require gsettings gtk-icon-cache freedesktop-desktop
require test-dbus-daemon
require meson

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Geary is an email application built around conversations, for the GNOME 3 desktop"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

# runs network tests
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-util/itstool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-spell/enchant:2[>=2.1]
        app-text/iso-codes
        base/libgee:0.8[>=0.8.5]
        core/json-glib[>=1.0]
        dev-db/sqlite:3[>=3.24]
        dev-libs/appstream-glib[>=0.7.10]
        dev-libs/glib:2[>=2.64]
        dev-libs/libhandy:1[>=0.90][gobject-introspection][vapi]
        dev-libs/libpeas:1.0[>=1.24.0]
        dev-libs/libsecret:1[>=0.11][gobject-introspection][vapi]
        dev-libs/libunwind[>=1.1]
        dev-libs/libxml2:2.0[>=2.7.8]
        gnome-desktop/gcr[>=3.10.1]
        gnome-desktop/gnome-online-accounts[gobject-introspection][vapi]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/gsound[gobject-introspection][vapi]
        gnome-desktop/gspell:1[gobject-introspection][vapi]
        gnome-desktop/libsoup:2.4[>=2.48]
        net-im/folks[>=0.11]
        net-libs/webkit:4.0[>=2.26]
        net-utils/gmime:3.0[>=3.2.4][gobject-introspection][vapi]
        x11-libs/gtk+:3[>=3.24.7]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcontractor=false
    -Dlibunwind_optional=false
    -Dprofile=default
    -Dref_tracking=false
    -Dtnef-support=false
    -Dvaladoc=false
)

geary_src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

geary_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

geary_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

