# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require gnome.org

SUMMARY="[DEPRECATED] Library to render UIs from a glade definition"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.18]
    build+run:
        dev-lang/python:* [[ note = [ required by libglade-convert script ] ]]
        dev-libs/atk[>=1.9.0]
        dev-libs/glib:2[>=2.10.0]
        dev-libs/libxml2:2.0[>=2.4.10]
        x11-libs/gtk+:2[>=2.5.0]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/libglade-convert_python3-compat.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --without-xml-catalog
)

src_prepare() {
    edo sed -i "/AC_PATH_PROG(PKG_CONFIG/d" configure.in
    autotools_src_prepare
}

src_test() {
    DISPLAY= \
        default
}

pkg_postinst() {
    einfo "Registering glade-2.0 DTD in system XML catalog ..."
    xmlcatalog --noout --add "system" "http://glade.gnome.org/glade-2.0.dtd" \
        "${ROOT}usr/share/xml/libglade/glade-2.0.dtd" "${ROOT}etc/xml/catalog"
    eend ${?}

    default
}

pkg_prerm() {
    einfo "Unregistering glade-2.0 DTD from system XML catalog ..."
    xmlcatalog --noout --del \
        "${ROOT}usr/share/xml/libglade/glade-2.0.dtd" "${ROOT}etc/xml/catalog"
    eend ${?}

    default
}

