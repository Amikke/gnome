# Copyright 2008 Stephen Bennett <spb@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic
require gnome.org [ suffix=tar.xz ] vala [ vala_dep=true with_opt=true ] meson

SUMMARY="A terminal emulator widget"
HOMEPAGE="https://developer.gnome.org/arch/gnome/widgets/vte.html"

LICENCES="GPL-2"
SLOT="2.91"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gnutls [[ description = [ Enable gnutls support (required for writing data encrypted to disk) ] ]]
    gobject-introspection
    gtk-doc
    systemd
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.19]
        sys-devel/gettext
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        dev-libs/fribidi[>=1.0.0]
        dev-libs/glib:2[>=2.52.0]
        dev-libs/icu:=[>=4.8]
        dev-libs/pcre2[>=10.21]
        sys-libs/zlib
        x11-libs/gtk+:3[>=3.20.0][gobject-introspection?]
        x11-libs/pango[>=1.22.0][gobject-introspection?]
        gnutls? ( dev-libs/gnutls[>=3.2.7] )
        systemd? ( sys-apps/systemd[>=220] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dglade=false'
    '-Dgtk3=true'
    '-Dicu=true'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    gnutls
    'gobject-introspection gir'
    'gtk-doc docs'
    'systemd _systemd'
    vapi
)

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Exherbo-fix-for-meson.patch
)

if [[ $(exhost --target) == *-musl* ]]; then
    DEFAULT_SRC_PREPARE_PATCHES=(
        "${FILES}"/0001-Add-W_EXITCODE-macro-for-non-glibc-systems.patch
    )
fi
